package com.example.de_market

import android.os.Bundle
import android.view.View
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.room.Room

//import androidx.room.Room
import com.example.de_market.ui.theme.De_marketTheme

class MainActivity : ComponentActivity() {
    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            RoomDb::class.java,
            "de_market.db"
        )
            .fallbackToDestructiveMigration()
            .build()
    //.addMigrations(RoomDb.migration1to2).build()
    }

    private val app by viewModels<App>()

    override fun onCreate(bundle: Bundle?) {
        theme.applyStyle(R.style.Theme_De_market, true)

        super.onCreate(bundle)

        theme.applyStyle(R.style.Theme_De_market, true)

        app.reset(db.dashboardItemDao)

        compose()
    }

    private fun compose() {
        setContent {
            De_marketTheme(darkTheme = app.darkTheme) {
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    app.windowInfo = rememberWindowInfo()
                    Navigation(app)
                }
            }
        }
    }
}