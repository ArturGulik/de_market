package com.example.de_market

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import kotlinx.coroutines.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject
import okhttp3.OkHttpClient
import okhttp3.Request

val GRID_WIDTH = 5
val GRID_HEIGHT = 20

enum class AppState {
    FAVORITES, HOME, ALL
}

class App: ViewModel() {
    var windowInfo: WindowInfo? = null

    var client: OkHttpClient = OkHttpClient()

    var dashboardItems by mutableStateOf(listOf<DashboardItem>())
        private set

    var marketItems by mutableStateOf(listOf<DashboardItem>())
        private set

    var selectedItem by mutableStateOf<DashboardItem?>(null)

    var grid by mutableStateOf(Array(GRID_WIDTH) { Array(GRID_HEIGHT) { false } })
        private set

    var editMode by mutableStateOf(false)
        private set

    var currentItem by mutableStateOf<DashboardItem?>(null)

    private var dashboardItemDao: DashboardItemDao? = null

    private var navController: NavHostController? = null

    var darkTheme by mutableStateOf(false)
        private set

    var appState: AppState by mutableStateOf(AppState.HOME)
        private set

    var searchWord by mutableStateOf("")

    var sortAscending by mutableStateOf(false)

    var sortType by mutableStateOf("price")
        private set

    var lastSyncDate by mutableStateOf("no info")

    override fun onCleared() {
        super.onCleared()
    }

    fun setNavController(controller: NavHostController) {
        if (navController == controller) {
            // navController didn't change, nothing to do
            return
        }
        navController = controller

        /*
        if (sideView() && navController!!.currentDestination?.route == Screen.DetailScreen.route) {
            navController!!.popBackStack()
        }

        if (!sideView() && currentTrail != null && appState != AppState.HOME)
            navController!!.navigate(Screen.DetailScreen.route)
         */
    }

    fun sideView() : Boolean {
        if (windowInfo == null) return false
        return windowInfo!!.screenWidthType != WindowInfo.WindowType.Compact
    }

    // The activity has been reset. Clear all outside references
    fun reset(newDashboardItemDao: DashboardItemDao) {
        windowInfo = null
        navController = null
        dashboardItemDao = newDashboardItemDao

        viewModelScope.launch(Dispatchers.IO) {
            if (dashboardItemDao!!.getDashboardItemCount() == 0) {
                // populate the database
                populateDashboardItemDao(dashboardItemDao!!)
            }

            dashboardItems = dashboardItemDao!!.getDashboardItems()
        }
    }

    fun openDetails(item: DashboardItem) {
        selectedItem = item
        navController?.navigate(Screen.DetailScreen.route)
    }

    fun goToFavorites() {
        appState = AppState.FAVORITES
    }

    fun goToWholeList() {
        appState = AppState.ALL
    }

    fun goHome() {
        appState = AppState.HOME
    }

    fun toggleDarkMode() {
        darkTheme = !darkTheme
    }

    @OptIn(DelicateCoroutinesApi::class)
    fun httpGet(url: String, onFinish: () -> Unit, onRetry: () -> Unit){
        val request = Request.Builder()
            .url(url)
            .build()

        GlobalScope.launch {
            // Delay for aesthetic reasons
            delay(500)
            client.newCall(request).enqueue(
                object : okhttp3.Callback {
                    override fun onFailure(call: okhttp3.Call, e: java.io.IOException) {
                        println("Failed to execute request")
                    }

                    override fun onResponse(call: okhttp3.Call, response: okhttp3.Response) {
                        val responseText: String = response.body!!.string()
                        val jsonObject: JsonObject =
                            Json.parseToJsonElement(responseText).jsonObject

                        if (jsonObject["results"] == null) {
                            println("No results found in JSON. Retrying")
                            onRetry()
                            httpGet(url, onFinish, onRetry)
                            return
                        }

                        val results: JsonArray = jsonObject["results"]!!.jsonArray
                        println(results)
                        println(results.isEmpty())

                        if (results.isEmpty()) {
                            println("Results is empty. Retrying")
                            onRetry()
                            httpGet(url, onFinish, onRetry)
                            return
                        }

                        marketItems = results.map { jsonToDashboardItem(it.jsonObject) }

                        onFinish()
                    }
                }
            )
        }
    }

    fun moveIfPossible(item: DashboardItem, changeX: Int, changeY: Int) {
        val fromX: Int = item.x
        val fromY: Int = item.y
        val width: Int = item.width
        val height: Int = item.height
        val destX: Int = fromX + changeX
        val destY: Int = fromY + changeY

        println("position read from the given item: ${item.x} ${item.y}")

        if (fromX == destX && fromY == destY) {
            println("Moving to the same position")
            return
        }
        if (destX + width > grid.size || destY + height > grid[0].size) {
            println("Moving out of bounds 1 : $destX + $width $destY + $height")
            return
        }
        if (destX < 0 || destY < 0) {
            println("Moving out of bounds 2: $destX $destY" )
            return
        }

        println("here 1")

        for (x in destX until destX + width) {
            for (y in destY until destY + height) {
                if (x >= fromX && x < fromX + width &&
                    y >= fromY && y < fromY + height) {
                    continue
                }
                if (grid[x][y]) return
            }
        }

        // Move is possible

        val newDashboardItems = dashboardItems.toMutableList()

        newDashboardItems.replaceAll { reItem ->
            println("GGLS ID: ${reItem.nameId}")
            if (item.nameId == reItem.nameId)
                reItem.moveCopy(destX, destY)
            else reItem
        }

        dashboardItems = newDashboardItems.toList()

        println("changed dashboardItems. current count is ${dashboardItems.count()} and the name is ${dashboardItems[0].name}")
        updateGrid()
    }

    fun updateGrid() {
        val newGrid = Array(GRID_WIDTH) { Array(GRID_HEIGHT) { false } }

        for (item in dashboardItems) {
            for (x in item.x until item.x + item.width) {
                for (y in item.y until item.y + item.height) {
                    newGrid[x][y] = true
                }
            }
        }

        grid = newGrid
    }

    fun deleteDashboardItem(it: DashboardItem) {
        val newDashboardItems = dashboardItems.toMutableList()
        val toRemove = newDashboardItems.find { fnd -> fnd.nameId == it.nameId }
        newDashboardItems.remove(toRemove)
        dashboardItems = newDashboardItems.toList()
        updateGrid()
    }

    fun addDashboardItem(it: DashboardItem): Boolean {
        var posX: Int? = null
        var posY: Int? = null

        updateGrid()

        var exitLoop: Boolean = false
        // Find free space for the new item
        for (y in 0 until grid[0].size) {
            if (exitLoop)
                break
            for (x in 0 until grid.size) {
                if (!grid[x][y]) {
                    posX = x
                    posY = y
                    exitLoop = true
                    break
                }

            }
        }
        if (posX == null) return false

        val newDashboardItems = dashboardItems.toMutableList()
        newDashboardItems.add(it.moveCopy(posX, posY, 1, 1))
        dashboardItems = newDashboardItems.toList()
        updateGrid()
        return true
    }

    fun toggleSortType() {
        sortType = if (sortType == "name") "price" else "name"
    }

    fun toggleEditMode() {
        editMode = !editMode
        if (editMode) {
            currentItem = null
            updateGrid()
        }
    }

    // Returns true if the item was successfully resized
    fun changeItemSize(it: DashboardItem, width: Int, height: Int): Boolean {
        if (it.x + width > grid.size || it.y + height > grid[0].size) {
            return false
        }
        if (it.x < 0 || it.y < 0) {
            return false
        }
        for (x in it.x until it.x + width) {
            for (y in it.y until it.y + height) {
                if (x >= it.x && x < it.x + it.width &&
                    y >= it.y && y < it.y + it.height) {
                    continue
                }
                if (grid[x][y]) return false
            }
        }

        val newDashboardItems = dashboardItems.toMutableList()
        newDashboardItems.replaceAll { reItem ->
            if (reItem == it)
                reItem.moveCopy(newWidth = width, newHeight = height).also { currentItem = it }
            else reItem
        }
        dashboardItems = newDashboardItems.toList()
        updateGrid()

        return true
    }
}