package com.example.de_market.mainScreen

import android.widget.Toast
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.de_market.App
import com.example.de_market.DashboardItem
import com.example.de_market.R
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.scale
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.times
import androidx.compose.ui.zIndex
import com.example.de_market.GRID_HEIGHT
import com.example.de_market.GRID_WIDTH
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

@Composable
fun DashboardScreen(app: App, innerPadding: PaddingValues) {
    val scale by animateFloatAsState(
        targetValue = if (app.editMode) 0.75f else 1f,
        animationSpec = tween(durationMillis = 500), label = ""
    )

    val editTransparency by animateFloatAsState(
        targetValue = if (app.editMode) 1f else 0f,
        animationSpec = tween(durationMillis = 500), label = ""
    )
    val editModeView: Boolean = (scale != 1f)

    Column(
        Modifier
            .fillMaxSize()
            .padding(innerPadding)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding((10 - 10 * editTransparency).dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .padding((10 - 10 * editTransparency).dp)
                    .fillMaxWidth(),
                text = if (editModeView) "Dashboard editor" else "de_market",
                textAlign = TextAlign.Center,
                fontSize = if (app.sideView()) 37.sp else 32.sp,
            )
        }
        DashboardContent(app, scale, editTransparency)
    }
}

class HoverInfo(var x: Int, var y: Int, var width: Int, var height: Int)

@Composable
fun GridBackground(app: App, cellSize: Float, gapSize: Int, hover: HoverInfo? = null, editTransparency: Float) {
    val density = LocalDensity.current
    val color = Color(Color.DarkGray.red, Color.DarkGray.green, Color.DarkGray.blue, editTransparency)

    Canvas(modifier = Modifier
        .fillMaxSize()
    ) {
        val cellSizePx = with(density) { cellSize.dp.toPx() }
        val gapSizePx = with(density) { gapSize.dp.toPx() }

        for (i in 0 until GRID_WIDTH) {
            for (j in 0 until GRID_HEIGHT) {
                val x = i * (cellSizePx + gapSizePx)
                if (hover != null && i >= hover.x && i < hover.x + hover.width && j >= hover.y && j < hover.y + hover.height) {
                    drawCircle(
                        color,
                        radius = 10.dp.toPx(),
                        center = Offset(3 * gapSizePx + x, gapSize + cellSizePx / 2 + j*(cellSizePx+gapSizePx))
                    )
                }
                else
                drawCircle(
                    color,
                    radius = 20.dp.toPx(),
                    center = Offset(3 * gapSizePx + x, gapSize + cellSizePx / 2 + j*(cellSizePx+gapSizePx))
                )
            }
        }
    }
}

@OptIn(DelicateCoroutinesApi::class)
@Composable
fun DashboardContent(app: App, scale: Float, editTransparency: Float) {
    val gapSize: Int = 15
    val squareSize: Float = (LocalConfiguration.current.screenWidthDp.toFloat() - gapSize * 6) / 5

    val hover = remember { mutableStateOf<HoverInfo?>(null) }
    var deleteDialogOpened by remember { mutableStateOf(false) }
    var deleteDialogItem by remember { mutableStateOf<DashboardItem?>(null) }

    val scrollState = rememberScrollState()

    val editModeView: Boolean = (scale != 1f)

    Box() {
        if (editModeView) {
            if (app.currentItem == null) {
                Text(
                    "You can drag and drop elements. Press and hold to remove, click to select an element and change its size.",
                    fontSize = 18.sp,
                    modifier = Modifier
                        .padding(horizontal = 15.dp)
                        .alpha(editTransparency)
                )
            }
            else {
                val context = LocalContext.current
                fun failedResize() {
                    Toast.makeText(context, "Not enough space for resizing", Toast.LENGTH_SHORT).show()
                }
                val iconSize = 60.dp

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .offset(y = (-12).dp)
                        .padding(horizontal = 15.dp)
                        .alpha(editTransparency),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    IconButton(
                        onClick = {
                            app.currentItem?.let {
                                app.changeItemSize(it, 1, 1)
                                    .also { success ->
                                        if (!success) failedResize()
                                    }
                            }
                        },
                        modifier = Modifier.size(iconSize).aspectRatio(1f)
                        ) {
                        Icon(
                            painter = painterResource(id = R.drawable.widget_1x1),
                            contentDescription = "",
                            tint = Color.Unspecified
                        )
                    }
                    IconButton(
                        onClick = {
                            app.currentItem?.let {
                                app.changeItemSize(it, 2, 2)
                                    .also { success ->
                                        if (!success) failedResize()
                                    }
                            }
                        },
                        modifier = Modifier.size(iconSize).aspectRatio(1f)
                        ) {

                        Icon(
                            painterResource(id = R.drawable.widget_2x2),
                            contentDescription = "",
                            tint = Color.Unspecified
                        )
                    }
                    IconButton(
                        onClick = {
                            app.currentItem?.let {
                                app.changeItemSize(it, 2, 3)
                                    .also { success ->
                                        if (!success) failedResize()
                                    }
                            }
                        },
                        modifier = Modifier.width(1.25 * iconSize).aspectRatio(1f)
                    ) {
                        Icon(
                            painterResource(id = R.drawable.widget_2x3),
                            contentDescription = "",
                            tint = Color.Unspecified
                        )
                    }
                    IconButton(
                        onClick = {
                            app.currentItem?.let {
                                app.changeItemSize(it, 5, 2)
                                    .also { success ->
                                        if (!success) failedResize()
                                    }
                            }
                        },
                        modifier = Modifier.height(1.75 * iconSize).aspectRatio(1f).width(iconSize*5)
                    ) {
                        Icon(
                            painterResource(id = R.drawable.widget_5x2),
                            contentDescription = "",
                            tint = Color.Unspecified
                        )
                    }
                }
            }
        }
        var boxHeight by remember { mutableFloatStateOf(0f) }
        Box(
            modifier = Modifier
                .fillMaxSize()
                .let {
                    if (editModeView) {
                        it
                            .scale(scale, scale)
                            .offset(y = (editTransparency * 15).dp)
                            .border(width = 5.dp, color = Color(0f, 0f, 0f, editTransparency))
                    } else it
                }
                .verticalScroll(
                    scrollState
                )
                .padding(top = (editTransparency * 15).dp)
                .padding(bottom = 15.dp)
                .onGloballyPositioned {
                    boxHeight = it.size.height.toFloat()
                }

        ) {
            var scrollbarOpacity by remember { mutableFloatStateOf(1f) }
            var scrollbarVisible by remember { mutableStateOf(false) }

            LaunchedEffect(scrollState.value) {
                scrollbarVisible = true
                scrollbarOpacity = 1f
                delay(2000)
                scrollbarVisible = false
                scrollbarOpacity = 0f
            }

            val animatedOpacity by animateFloatAsState(
                targetValue = scrollbarOpacity,
                animationSpec = tween(durationMillis = 500), label = ""
            )

            val scrollbarHeight = 50.dp
            val percent: Float = if (scrollState.maxValue == 0) 0f else scrollState.value.toFloat() / scrollState.maxValue
            val scrollbarOffset = with(LocalDensity.current) {
                ((-15).dp.toPx() + percent * (boxHeight-(20.dp).toPx())).toDp()
            }

            Box(
                modifier = Modifier
                    //.offset(x = -(30).dp, y = (-15).dp + scrollbarPosition)
                    .offset(x = -(30).dp, y = scrollbarOffset)
                    .height(scrollbarHeight)
                    .width(20.dp)
                    .alpha(animatedOpacity)
                    .background(
                        if (app.darkTheme) Color(0.2f, 0.2f, 0.3f) else Color.DarkGray,
                        shape = RoundedCornerShape(10.dp)
                    )
//                    .pointerInput(Unit) {
//                        detectDragGestures(
//                            onDrag = { change, dragAmount ->
//                                if (!scrollbarVisible) return@detectDragGestures
//                                change.consume()
//                                val newScrollValue: Int = scrollState.value + (dragAmount.y.toInt() * 1.3f).roundToInt()
//                                GlobalScope.launch {
//                                    scrollState.scrollTo(
//                                        newScrollValue.coerceIn(
//                                            0,
//                                            scrollState.maxValue
//                                        )
//                                    )
//                                }
//                            }
//                        )
//                    }
            ) {}
            if (editModeView) {
                GridBackground(app, squareSize, gapSize, hover.value, editTransparency)
            }
            app.dashboardItems.forEach { item ->
                DashboardElement(
                    app, item, gapSize = gapSize,
                    changeHover = if (app.editMode) ({ hover.value = it }) else null,
                    openDeleteDialog = if (app.editMode) ({
                        deleteDialogOpened = true
                        deleteDialogItem = it
                    }) else null,
                    scrollState = scrollState)
            }

            // Spacer used to reveal the entire grid - useful in edit mode for
            // easier dragging of elements
            if (editModeView) {
                Spacer(
                    modifier = Modifier
                        .width(15.dp)
                        .height((GRID_HEIGHT * (squareSize + gapSize) - gapSize).dp)
                )
            }
        }
        if (editModeView) {
            Button(
                modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .padding(vertical = 4.dp)
                    .padding(end = 15.dp)
                    .alpha(editTransparency),
                onClick = {
                app.toggleEditMode()
                hover.value = null
            }) {
                Text("Save changes")
            }
        }
    }
    if (deleteDialogOpened) {
        AlertDialog(
            onDismissRequest = {
                deleteDialogOpened = false
            },
            title = {
                Text("Removing an element")
            },
            text = {
                Text("Are you sure you want to remove '${deleteDialogItem?.name}' from the dashboard?")
            },
            confirmButton = {
                Button(
                    onClick = {
                        deleteDialogOpened = false
                        deleteDialogItem?.let { app.deleteDashboardItem(it) }
                    }
                ) {
                    Text("Yes, remove it!")
                }
            },
            dismissButton = {
                Button(
                    onClick = {
                        deleteDialogOpened = false
                    }
                ) {
                    Text("Cancel")
                }
            }
        )
    }
}

@OptIn(ExperimentalFoundationApi::class, DelicateCoroutinesApi::class)
@Composable
fun DashboardElement(app: App, item: DashboardItem, gapSize: Int,
                     changeHover: ((HoverInfo?) -> Unit)? = null,
                     openDeleteDialog: ((DashboardItem) -> Unit)? = null,
                     scrollState: ScrollState? = null,
                     disableLongClick: Boolean = false) {
    val context = LocalContext.current
    val squareSize: Float = (LocalConfiguration.current.screenWidthDp.toFloat() - gapSize * 6) / 5

    val squarePx = LocalDensity.current.run { (squareSize + gapSize).dp.toPx() }

    var zIndex by remember { mutableFloatStateOf(0f) }
    var offsetX by remember { mutableFloatStateOf(0f) }
    var offsetY by remember { mutableFloatStateOf(0f) }
    var alpha by remember { mutableFloatStateOf(1f) }

    val leftPadding = max(0f, gapSize + (item.x) * gapSize + item.x * squareSize)
    val rightPadding = max(0f, -1 * ((item.x) * gapSize + item.x * squareSize))

    val topPadding = max(0f, (item.y) * gapSize + item.y * squareSize)
    val bottomPadding = max(0f, -1 * ((item.y) * gapSize + item.y * squareSize))

    var currentItem by remember { mutableStateOf(item) }
    // Update the current item when the item changes
    LaunchedEffect(item) {
        currentItem = item
    }

    var scrollJob: Job? = null

    var dragSum by remember { mutableFloatStateOf(0f) }

    val screenHeight = LocalConfiguration.current.screenHeightDp

    var scrollSpeed: Float = 15f

    var absoluteY by remember { mutableFloatStateOf(0f) }

    Box(
        Modifier
            .zIndex(zIndex)
            .offset { IntOffset(offsetX.roundToInt(), offsetY.roundToInt()) }
            .padding(
                start = leftPadding.dp,
                end = rightPadding.dp,
                top = topPadding.dp,
                bottom = bottomPadding.dp
            )
            .width((item.width * squareSize + (item.width - 1) * gapSize).dp)
            .height((item.height * squareSize + (item.height - 1) * gapSize).dp)
            .clip(RoundedCornerShape(10.dp))
            .alpha(alpha)
            .background(if (app.darkTheme) Color(0.2f, 0.2f, 0.3f) else Color.LightGray)
            .let { modifier ->
                if (changeHover != null && openDeleteDialog != null) {
                    modifier.pointerInput(Unit) {
                        detectDragGestures(
                            onDragEnd = {
                                scrollJob?.cancel()

                                val changeX = (offsetX / squarePx).roundToInt()
                                val changeY = (offsetY / squarePx).roundToInt()

                                app.moveIfPossible(currentItem, changeX, changeY)

                                zIndex = 0f
                                offsetX = 0f
                                offsetY = 0f
                                alpha = 1f
                                dragSum = 0f
                                changeHover(null)
                            },
                            onDragStart = {
                                app.currentItem = null
                            },
                            onDrag = { change, dragAmount ->
                                change.consume()
                                zIndex = 1f
                                offsetX += dragAmount.x
                                offsetY += dragAmount.y
                                alpha = 0.5f

                                changeHover(
                                    HoverInfo(
                                        x = currentItem.x + (offsetX / squarePx).roundToInt(),
                                        y = currentItem.y + (offsetY / squarePx).roundToInt(),
                                        width = currentItem.width,
                                        height = currentItem.height
                                    )
                                )
                                if (scrollState == null) return@detectDragGestures

                                // Scroll the screen if the element is dragged to the edge
                                if (absoluteY.toDp() < (screenHeight * 0.3).dp) {
                                    if (absoluteY.toDp() < (screenHeight * 0.22).dp)
                                        scrollSpeed = 30f
                                    else scrollSpeed = 12f

                                    scrollJob?.cancel()
                                    scrollJob = GlobalScope.launch {
                                        while (true) {
                                            if (!scrollState.canScrollBackward) {
                                                scrollJob?.cancel()
                                                return@launch
                                            }
                                            scrollState.scrollBy(-scrollSpeed)
                                            offsetY -= scrollSpeed

                                            delay(16)
                                        }
                                    }
                                } else if (absoluteY.toDp() > (screenHeight * 0.6).dp) {
                                    if (absoluteY.toDp() > (screenHeight * 0.68).dp)
                                        scrollSpeed = 30f
                                    else scrollSpeed = 12f

                                    scrollJob?.cancel()
                                    scrollJob = GlobalScope.launch {
                                        while (true) {
                                            if (!scrollState.canScrollForward) {
                                                scrollJob?.cancel()
                                                return@launch
                                            }
                                            scrollState.scrollBy(scrollSpeed)
                                            offsetY += scrollSpeed

                                            delay(16)
                                        }
                                    }
                                } else {
                                    scrollJob?.cancel()
                                }
                            }
                        )
                    }
                } else modifier
            }
            .combinedClickable(
                onClick = {
                    if (!app.editMode)
                        app.openDetails(currentItem)
                    else {
                        if (disableLongClick)
                            return@combinedClickable
                        if (app.currentItem != currentItem)
                            app.currentItem = currentItem
                        else app.currentItem = null
                    }
                },
                onLongClick = {
                    if (disableLongClick)
                        return@combinedClickable
                    if (!app.editMode) {
                        app.toggleEditMode()
                    }
                    else if (openDeleteDialog != null) {
                        openDeleteDialog(currentItem)
                    }
                }
            )
            .onGloballyPositioned {
                absoluteY = it.localToRoot(Offset(0f, 0f)).y
            }

    ) {
        // switch statement to match predefined item widths and heights
        when ("${item.width}x${item.height}") {
            "1x1" -> DashboardElement1x1(item)
            "2x2" -> DashboardElement2x2(item)
            "2x3" -> DashboardElement2x3(item)
            "5x2" -> DashboardElement5x2(item)
            else -> DashboardElement1x1(item)
        }
    }
}

@Composable
fun DashboardElement2x3(item: DashboardItem) {
    Column (
        Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column (
            Modifier.fillMaxWidth().padding(top = 5.dp).weight(1f),
            verticalArrangement = Arrangement.SpaceAround,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            if (item.name.length < 20) {
                Text(
                    text = item.name, fontSize = 20.sp,
                    textAlign = TextAlign.Center,
                    overflow = TextOverflow.Ellipsis, maxLines = 2
                )
            }
            Text(
                item.sellPrice, fontSize = if (item.name.length < 20) 28.sp else 30.sp,
                overflow = TextOverflow.Visible
            )
        }
        AsyncImage(
            modifier = Modifier.fillMaxWidth().weight(1.5f).background(Color.Transparent),
            model = "https://community.akamai.steamstatic.com/economy/image/${item.imageUrl}/128fx128f",
            contentDescription = ""
        )
    }
}

@Composable
fun DashboardElement1x1(item: DashboardItem) {
    AsyncImage(
        modifier = Modifier.fillMaxHeight(),
        model = "https://community.akamai.steamstatic.com/economy/image/${item.imageUrl}/128fx128f",
        contentDescription = ""
    )
}

@Composable
fun DashboardElement2x2(item: DashboardItem) {
    AsyncImage(
        modifier = Modifier.fillMaxHeight(),
        model = "https://community.akamai.steamstatic.com/economy/image/${item.imageUrl}/256fx256f",
        contentDescription = ""
    )
}

@Composable
fun DashboardElement5x2(item: DashboardItem) {
    Row(
        Modifier.fillMaxSize()
    ) {
        Box() {
            AsyncImage(
                modifier = Modifier.fillMaxHeight(),
                model = "https://community.akamai.steamstatic.com/economy/image/${item.imageUrl}/256fx256f",
                contentDescription = ""
            )
        }
        Column(
            Modifier
                .padding(start = 10.dp)
                .fillMaxHeight()
                .weight(1f),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
                Text(text = item.name, overflow = TextOverflow.Ellipsis, fontSize = 25.sp)
                Text(item.sellPrice, fontSize = 25.sp, modifier = Modifier.padding(bottom = 10.dp))
        }
    }
}