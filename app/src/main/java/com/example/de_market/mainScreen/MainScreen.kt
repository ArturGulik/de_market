package com.example.de_market.mainScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.de_market.App

@Composable
fun MainAppScreen(app: App, viewId: Int, innerPadding: PaddingValues, onHome: () -> Unit) {
    when(viewId) {
        0 -> {
            MarketBrowsingScreen(app = app, innerPadding = innerPadding)
        }
        1 -> {
            DashboardScreen(app, innerPadding = innerPadding)
        }
        2 -> {
            SettingsScreen(app = app, innerPadding = innerPadding, onHome = onHome)
        }
    }
}

@Composable
fun MyBottomAppBar(
    current: Int,
    app: App,
    onFavorite: () -> Unit,
    onHome: () -> Unit,
    onAll: () -> Unit
) {
    BottomAppBar(
        modifier = Modifier.height(50.dp),
        contentPadding = PaddingValues(0.dp),
        actions = {
            val selectedColor = if (app.darkTheme) Color(0xff3700b3) else Color.LightGray
            Row(
                modifier = Modifier.fillMaxSize(),
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                IconButton(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(1f)
                        .background(if (current == 0) selectedColor else Color.Transparent),
                    onClick = onFavorite
                ) {
                    Icon(
                        Icons.Filled.Menu,
                        contentDescription = "",
                    )
                }
                IconButton(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(1f)
                        .background(if (current == 1) selectedColor else Color.Transparent),
                    onClick = onHome
                ) {
                    Icon(Icons.Filled.Home, contentDescription = "")
                }
                IconButton(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(1f)
                        .background(if (current == 2) selectedColor else Color.Transparent),
                    onClick = onAll
                ) {
                    Icon(
                        Icons.Filled.Settings,
                        contentDescription = "",
                    )
                }
            }
        }
    )
}