package com.example.de_market.mainScreen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.de_market.App

@Composable
fun MarketBrowsingScreen(app: App, innerPadding: PaddingValues) {
    Row(
        modifier = Modifier.padding(innerPadding),
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {
            Text(
                modifier = Modifier.padding(15.dp),
                text = "Browse the Market",
                textAlign = TextAlign.Center,
                fontSize = if (app.sideView()) 25.sp else 32.sp,
            )
            Row(
                modifier = Modifier
                    .padding(10.dp)
                    .padding(horizontal = 15.dp)
                    .padding(bottom = 15.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Icon(
                    Icons.Filled.Search,
                    contentDescription = "",
                    modifier = Modifier.size(30.dp)
                )
                // Text input
                TextField(
                    value = app.searchWord,
                    onValueChange = { app.searchWord = it },
                    modifier = Modifier
                        .fillMaxWidth(0.75f)
                        .height(50.dp)
                        .padding(horizontal = 10.dp),
                    maxLines = 1,
                )
                IconButton(onClick = { app.toggleSortType() }) {
                    Icon(
                        imageVector = if (app.sortType == "price")
                            Icons.Filled.ShoppingCart
                        else Icons.Filled.Info,
                        contentDescription = "",
                        modifier = Modifier.size(30.dp)
                    )
                }
                IconButton(onClick = { app.sortAscending = !app.sortAscending } ) {
                    Icon(
                        imageVector = if (app.sortAscending)
                            Icons.Filled.KeyboardArrowUp
                        else Icons.Filled.KeyboardArrowDown,
                        contentDescription = "",
                        modifier = Modifier.size(30.dp)
                    )
                }
            }
            LazyVerticalGrid(
                verticalArrangement = Arrangement.spacedBy(15.dp),
                columns = GridCells.Fixed(1),
                modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = 15.dp)

            ) {
                app.marketItems
                    .filter { it.foundString(app.searchWord) }
                    .let { itemsToSort ->
                        if (app.sortType == "price")
                            itemsToSort.sortedBy { it.getPrice() }
                        else itemsToSort.sortedBy { it.name }
                    }
                    .let { list ->
                        if (!app.sortAscending) list.reversed() else list
                    }
                    .forEach { item ->
                    item {
                        DashboardElement(app, item, gapSize = 0, disableLongClick = true)
                    }
                }
            }
        }
        if (app.sideView()) {
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            ) {
                //DetailScreenCompose(app = app, onlyShowFavorite=true)
            }
        }
    }
}