package com.example.de_market.mainScreen

import android.widget.Toast
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import com.example.de_market.App
import com.example.de_market.R
import com.example.de_market.currentDateString
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.min

@OptIn(DelicateCoroutinesApi::class)
@Composable
fun SettingsScreen(app: App, innerPadding: PaddingValues, onHome: () -> Unit) {
    val context = LocalContext.current
    var showDialog by remember { mutableStateOf(false) }
    var dialogText by remember { mutableStateOf("") }

    val iconBgColor = if (app.darkTheme) Color(0.2f, 0.2f, 0.3f) else Color.LightGray

    Row(
        modifier = Modifier.padding(innerPadding),
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {
            Text(
                modifier = Modifier.padding(15.dp),
                text = "Settings",
                textAlign = TextAlign.Center,
                fontSize = if (app.sideView()) 25.sp else 32.sp,
            )

            Row(
                modifier = Modifier.fillMaxWidth(0.9f),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text("Toggle color mode:", fontSize = 25.sp, modifier = Modifier.padding(10.dp))
                IconButton(
                    modifier = Modifier
                        .padding(10.dp)
                        .background(iconBgColor, shape = RoundedCornerShape(20.dp))
                        .padding(10.dp),
                    onClick = { app.toggleDarkMode() }
                ) {
                    Icon(
                        painterResource(id = if (app.darkTheme) R.drawable.dark_mode else R.drawable.light_mode),
                        modifier = Modifier.size(60.dp),
                        contentDescription = "",
                    )
                }
            }

            Row(
                modifier = Modifier.fillMaxWidth(0.9f),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text("Customize dashboard:", fontSize = 25.sp, modifier = Modifier.padding(10.dp))
                IconButton(
                    modifier = Modifier
                        .padding(10.dp)
                        .background(iconBgColor, shape = RoundedCornerShape(20.dp))
                        .padding(10.dp),
                    onClick = {
                        if (!app.editMode) {
                            GlobalScope.launch {
                                delay(200)
                                app.toggleEditMode()
                            }
                        }
                        else app.toggleEditMode()
                        onHome()
                    }
                ) {
                    Icon(
                        imageVector = Icons.Filled.Edit,
                        modifier = Modifier.size(60.dp),
                        contentDescription = "",
                    )
                }
            }

            Row(
                modifier = Modifier
                    .padding(vertical = 10.dp)
                    .fillMaxWidth(0.9f)
                    .height(2.dp)
                    .background(Color.Gray)
            ){}

            Text("Database synchronization", fontSize = 25.sp, modifier = Modifier.padding(10.dp))
            Text("Last synchronization: ${app.lastSyncDate}", fontSize = 20.sp, modifier = Modifier)

            Button(
                onClick = {
                    dialogText = "Contacting the API"
                    showDialog = true
                    app.httpGet(
                        url = "https://steamcommunity.com/market/search/render/?query=&start=0&count=50&search_descriptions=0&sort_dir=desc&appid=730&norender=1&sort_column=popular",
                        onRetry = {
                            dialogText = "Invalid response, retrying"
                        },
                        onFinish = {
                            dialogText = "Synchronization complete"
                            GlobalScope.launch {
                                delay(500)
                                showDialog = false
                                app.syncedNow()
                            }
                        }
                    )
                },
                modifier = Modifier.padding(10.dp)
            ) {
                Text(text="Synchronize now", modifier = Modifier, fontSize = 25.sp)
            }
        }
        if (app.sideView()) {
        }
    }
    if (showDialog) {
        Dialog(onDismissRequest = {
            Toast.makeText(context, "Please wait for the synchronization to complete", Toast.LENGTH_SHORT).show()
        }) {
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp)
                    .padding(16.dp),
                shape = RoundedCornerShape(16.dp),
            ) {
                Row(
                    Modifier.fillMaxSize(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = dialogText,
                        fontSize = 25.sp,
                        modifier = Modifier,
                        textAlign = TextAlign.Center,
                    )
                    if (dialogText != "Synchronization complete")
                        AnimatedDots()
                }
            }
        }
    }

}

private fun App.syncedNow() {
    lastSyncDate = currentDateString()
}

@Composable
fun AnimatedDots() {
    val infiniteTransition = rememberInfiniteTransition(label = "")
    val alpha by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 1.5f,
        animationSpec = infiniteRepeatable(
            animation = tween(1200, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        ), label = ""
    )

    Text(
        text = ".",
        modifier = Modifier.alpha(min(alpha * 3f, 1f)),
        fontSize = 25.sp
    )
    Text(
        text = ".",
        modifier = Modifier.alpha(min((alpha - 0.33f) * 2f, 1f)),
        fontSize = 25.sp
    )
    Text(
        text = ".",
        modifier = Modifier.alpha(min((alpha - 0.66f), 1f)),
        fontSize = 25.sp
    )
}