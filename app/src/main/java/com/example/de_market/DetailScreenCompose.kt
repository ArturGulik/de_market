package com.example.de_market

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.de_market.mainScreen.DashboardContent

@Composable
fun DetailScreenCompose(app: App, item: DashboardItem?) {
    val itemColor = Color(if (item?.nameColor != null)
        android.graphics.Color.parseColor("#" + item.nameColor)
    else android.graphics.Color.parseColor("#ffffff"))

    Column(
        Modifier
            .fillMaxSize()
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color(android.graphics.Color.parseColor("#1c1b1f")))
            ,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                modifier = Modifier
                    .padding(10.dp)
                    .fillMaxWidth(),
                text = item?.name ?: "No item selected",
                color = itemColor,
                textAlign = TextAlign.Center,
                fontSize = if (app.sideView()) 32.sp else 28.sp,
            )
        }
        Text(text = "Item type: " + (item?.type ?: "unknown"), modifier = Modifier.padding(10.dp), fontSize = 20.sp)
        Text(text = "Buy price: " + (item?.sellPrice ?: ""), modifier = Modifier.padding(10.dp), fontSize = 20.sp)
        Text(text = "Sell price: " + (item?.salePrice ?: ""), modifier = Modifier.padding(10.dp), fontSize = 20.sp)
        AsyncImage(
            modifier = Modifier.fillMaxWidth(0.5f).align(Alignment.CenterHorizontally),
            model = "https://community.akamai.steamstatic.com/economy/image/${item?.imageUrl}/256fx256f",
            contentDescription = ""
        )

        val context = LocalContext.current
        // Button to add item to dashboard or delete it
        Button(
            onClick = {
                if (app.dashboardItems.find{ it.nameId == item!!.nameId } != null) {
                    app.deleteDashboardItem(item!!)
                } else {
                    val success = app.addDashboardItem(item!!)
                    if (!success)
                        Toast.makeText(context, "No empty space on the dashboard", Toast.LENGTH_SHORT).show()
                }
            },
            modifier = Modifier.padding(10.dp).align(Alignment.CenterHorizontally)
        ) {
            Text(
                text = if (app.dashboardItems.find{ it.nameId == item!!.nameId } != null) "Remove from dashboard" else "Add to dashboard",
                fontSize = 22.sp
            )
        }
    }
}