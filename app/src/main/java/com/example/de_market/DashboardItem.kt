package com.example.de_market

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.ProvidedTypeConverter
import androidx.room.Query
import androidx.room.TypeConverter
import androidx.room.Upsert
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.jsonObject

@Entity(tableName = "dashboard_items")
data class DashboardItem(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    var name: String,
    var nameColor: String,
    var type: String,
    var description: String,
    var imageUrl: String,
    var nameId: String,

    var appId: String,
    var appName: String,
    var appImageUrl: String,

    var sellPrice: String,
    var salePrice: String,

    var x: Int = 0,
    var y: Int = 0,
    var width: Int = 0,
    var height: Int = 0,

    var tabletX: Int = 0,
    var tabletY: Int = 0,
    var tabletWidth: Int = 0,
    var tabletHeight: Int = 0
) {
    fun moveCopy(destX: Int? = null, destY: Int? = null, newWidth: Int? = null, newHeight: Int? = null): DashboardItem {
        return DashboardItem(
            id = id,
            name = name,
            nameColor = nameColor,
            type = type,
            description = description,
            imageUrl = imageUrl,
            nameId = nameId,
            appId = appId,
            appName = appName,
            appImageUrl = appImageUrl,
            sellPrice = sellPrice,
            salePrice = salePrice,
            x = destX ?: x,
            y = destY ?: y,
            width = newWidth ?: width,
            height = newHeight ?: height,
            tabletX = tabletX,
            tabletY = tabletY,
            tabletWidth = tabletWidth,
            tabletHeight = tabletHeight
        )
    }
    fun foundString(searchWord: String): Boolean {
        if (searchWord.isEmpty()) return true
        if (name.contains(searchWord, ignoreCase = true)) return true
        if (description.contains(searchWord, ignoreCase = true)) return true
        return false
    }

    fun getPrice(): Float {
        return sellPrice.removePrefix("$").toFloat()
    }
}

fun jsonToDashboardItem(json: JsonObject, width: Int = 5, height: Int = 2): DashboardItem {
    return DashboardItem(
        name = json["name"].toString().removeSurrounding("\""),
        nameId = json["asset_description"]!!.jsonObject["classid"].toString().removeSurrounding("\""),
        appId = json["asset_description"]!!.jsonObject["appid"].toString().removeSurrounding("\""),
        nameColor = json["asset_description"]!!.jsonObject["name_color"].toString().removeSurrounding("\""),
        type = json["asset_description"]!!.jsonObject["type"].toString().removeSurrounding("\""),
        appImageUrl = json["app_icon"].toString().removeSurrounding("\""),
        sellPrice = json["sell_price_text"].toString().removeSurrounding("\""),
        salePrice = json["sale_price_text"].toString().removeSurrounding("\""),
        appName = json["app_name"].toString().removeSurrounding("\""),
        imageUrl = json["asset_description"]!!.jsonObject["icon_url"].toString().removeSurrounding("\""),
        description = "",
        x = 0,
        y = 0,
        width = width,
        height = height,
        tabletX = 0,
        tabletY = 0,
        tabletWidth = width,
        tabletHeight = height
    )
}

@Dao
interface DashboardItemDao {
    @Upsert
    suspend fun upsertDashboardItem(dashboardItem: DashboardItem)

    @Delete
    suspend fun deleteDashboardItem(dashboardItem: DashboardItem)

    @Query("DELETE FROM dashboard_items")
    fun clear()

    @Query("SELECT * FROM dashboard_items")
    fun getDashboardItems() : List<DashboardItem>

    @Query("SELECT COUNT(*) FROM dashboard_items")
    fun getDashboardItemCount(): Int
}

suspend fun populateDashboardItemDao(dao: DashboardItemDao) {
    dao.clear()

    dao.upsertDashboardItem(DashboardItem(
        name = "★ Ursus Knife | Urban Masked (Well-Worn)",
        nameColor = "8650AC",
        type = "★ Covert Knife",
        description = "",
        imageUrl = "-9a81dlWLwJ2UUGcVs_nsVtzdOEdtWwKGZZLQHTxDZ7I56KU0Zwwo4NUX4oFJZEHLbXH5ApeO4YmlhxYQknCRvCo04DEVlxkKgpovbSsLQJfxuHbZC59_8yJgIGbksjhIbnQmFRc7cF4n-SP9NTz3gKy-EFqYD-ido_DewY3MFiB-Ae2wu3r0cO6tMnPzXVquyIj7WGdwULEFYZVug",
        nameId = "name1",
        appId = "730",
        appName = "Counter-Strike 2",
        appImageUrl = "https://cdn.akamai.steamstatic.com/steamcommunity/public/images/apps/730/8dbc71957312bbd3baea65848b545be9eae2a355.jpg",
        sellPrice = "$154.10",
        salePrice = "$147.10",
        x = 0,
        y = 0,
        width = 5,
        height = 2,
        tabletX = 0,
        tabletY = 0,
        tabletWidth = 5,
        tabletHeight = 2
        ))
}