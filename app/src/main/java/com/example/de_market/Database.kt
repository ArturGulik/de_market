package com.example.de_market

import android.content.Context
import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import androidx.room.migration.AutoMigrationSpec
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(
    entities = [DashboardItem::class],
    version = 2,
    autoMigrations = [
    ]
)

abstract class RoomDb : RoomDatabase() {

    abstract val dashboardItemDao: DashboardItemDao
    //abstract val otherDao: OtherDaoClass

    companion object {
        /*
        val migration1to2 = object : Migration(1, 2) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("CREATE TABLE IF NOT EXISTS timer_entries" +
                        " (trailId INTEGER NOT NULL, id INTEGER NOT NULL PRIMARY KEY)")
            }
        }
         */
    }

}