package com.example.de_market

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable

abstract class Screen(var route: String) {
    data object HomeScreen : Screen("home")
    data object DetailScreen : Screen("detail_screen")

    @Composable
    open fun Compose() {
        Text (
            text = "This screen's content is not created yet"
        )
    }
}