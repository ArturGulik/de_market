package com.example.de_market

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

fun currentDateString() : String {
    val date = Calendar.getInstance().time
    return SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault()).format(date)
}